import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket'
import App from './App.vue';
import router from './router';
import store from './store';
import "./style/main.scss";

import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faCoffee);

Vue.component('font-awesome-icon', FontAwesomeIcon);

var loc = window.location, uri;
if (loc.protocol === "https:"){
    uri = "wss:";
}else{
    uri = "ws:";
}
uri += "//" + loc.host;
uri += "/ws";


Vue.use(VueNativeSock, uri, {
    format: 'json',
    connectManually: true,
});

Vue.config.productionTip = false;

new Vue({
    created: function(){
        this.$store.dispatch('init',{
            connect: this.$connect,
            disconnect: this.$disconnect,
        });
    },
    router,
    store,
    render: h => h(App)
}).$mount('#app');
