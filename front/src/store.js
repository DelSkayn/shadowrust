import Vue from 'vue';
import Vuex from 'vuex';
import Api from "./api";
import router from "./router";

Vue.use(Vuex);

export default new Vuex.Store({
state: {
    username: "",
    is_logged_in: false,
    is_sidebar_open: false,
    error: null,
    connect: null,
    disconnect: null,
},
mutations: {
    updateUsername(state,name){
        state.username = name;
    },
    setError(state,error){
        state.error = error;
    },
    clearError(state){
        state.error = null;
    },
    toggleSidebar(state){
        state.is_sidebar_open != state.is_sidebar_open;
    },
    setLogin(state, value){
        state.is_logged_in = value;
        var pre_login = ["login","register"];
        if(value){
            state.connect();
            if (pre_login.includes(router.currentRoute.name)){
                router.push("/home");
            }
        }else{
            state.disconnect();
            if (!pre_login.includes(router.currentRoute.name)){
                router.push("/");
            }
        }
    },
    setFunctions(state,{connect,disconnect}){
        state.connect = connect;
        state.disconnect = disconnect;
    }
},
methods: {
},
actions: {
    init({commit},value){
        commit('setFunctions',value);
        Api.userInfo().then((data) => {
            commit('updateUsername',data.username);
            commit('setLogin', true);
        }).catch(() => {});
    },
    login({commit,state},{on_success,password}){
        console.log(password);
        Api.login(state.username,password)
            .then(function(res) {
                if(res.success){
                    if(on_success !== undefined){
                        commit('setLogin',true);
                        on_success();
                    }
                }else{
                    commit('setError',res.error);
                }
                console.log(res);
            }).catch(function (e){
                //commit('setError', "Received error from the server: " + e.statusText);
                console.log(e);
            });
    },
    logout({state,commit},on_success){
        console.log(state);
        console.log(on_success);
        Api.logout()
            .then(function(res){
                if(res.success){
                    if(on_success != null){
                        commit("setLogin", false);
                        on_success();
                    }
                }
            });
    },
    register({commit,state},{password, on_success}){
        Api.register(state.username,password)
            .then(function(res){
                if (res.success){
                    console.log(res);
                    if (on_success != null){
                        on_success();
                    }
                }else{
                    commit("setError",res.error);
                }
            }).catch(function (e) {
                console.log(e)
            });
    }
},
strict: process.env.NODE_ENV !== 'production'
});
