function request(method,url,data){
    return new Promise(function(resolve,reject){
        var xhr = new XMLHttpRequest();
        xhr.open(method,"/api/" + url,true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300){
                resolve(JSON.parse(xhr.response));
            }else{
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send(JSON.stringify(data));
    });
}

export default {
    userInfo(){
        return request("GET","login/info");
    },
    login (name,password){
        return request("POST","login",{username: name, password: password});
    },
    register(name,password){
        return request("POST","register",{username: name, password: password});
    },
    available(name){
        return request("POST","register/available",{username: name});
    },
    logout(){
        return request("GET","logout");
    },
    isLoggedIn(){
        return request("GET","login");
    }
}
