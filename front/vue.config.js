module.exports = {
    devServer: {
        proxy: {
            "/api": {
                target: "http://localhost:3030",
            },
            "/ws": {
                target: "ws://localhost:3030",
                ws: true,
            }
        }
    }
}
