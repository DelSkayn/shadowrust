use actix_web::{ws, HttpRequest, HttpResponse,Error};
use actix_web::middleware::session::RequestSession;
use serde::{Serialize,Deserialize};
use serde_json::Value;
use ::actix::prelude::*;
use crate::{user,AppState};
use crate::db::Db;

use std::time::{Instant,Duration};
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

struct Websocket{
    username: String,
    hb: Instant,
}

impl Websocket{
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self,AppState>){
        ctx.run_interval(HEARTBEAT_INTERVAL, |act,ctx|{
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT{
                trace!("websocket dropping client for timeout.");
                ctx.stop();
                return;
            }
            ctx.ping("");
        });
    }
}

pub struct Ctx{
    username: String,
    db: Db,
}


impl Actor for Websocket{
    type Context = ws::WebsocketContext<Self,AppState>;

    fn started(&mut self, ctx: &mut Self::Context){
        trace!("websocket client connect.");
        self.hb(ctx);
    }
}

impl StreamHandler<ws::Message, ws::ProtocolError> for Websocket{
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context){
        trace!("websocket message: {:?}",msg);
        self.hb = Instant::now();
        match msg {
            ws::Message::Ping(x) => {
                ctx.pong(&x);
            }
            ws::Message::Pong(_) => {
            }
            ws::Message::Text(x) => {
                ctx.text(x);
            }
            ws::Message::Binary(x) => {
                ctx.binary(x);
            }
            ws::Message::Close(_) => {
                trace!("websocket client disconnect.");
                ctx.stop();
            }
        }
    }
}

pub fn websocket(req: &HttpRequest<AppState>) -> Result<HttpResponse,Error>{
    user::authenticate(req,|req|{
        let username = req.session()
            .get("username")
            .unwrap()
            .unwrap();
        ws::start(&req,Websocket{
            username,
            hb: Instant::now(),
        })
    })
}
