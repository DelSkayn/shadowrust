
pub trait WebsocketFunction<'a>{
    const NAME: &'static str;
    type Result: Serialize;
    type Args: Deserialize<'a>;

    fn execute(context: &Ctx, args: Self::Args) -> Self::Result;
}

#[derive(Deserialize)]
pub struct WebsocketFunctionCall<'a>{
    id: i32,
    name: &'a str,
    args: Value,
}

#[derive(Serialize)]
pub enum WebsocketResult{
    Function{
        id: i32,
        result: Value,
    },
    Message(Message),
}

#[derive(Serialize)]
pub enum Message{
    Join{
        username: String,
    },
}
