use ::actix::prelude::*;
use ::actix_web::{
    Json,
    State,
    HttpRequest,
    HttpResponse,
    HttpMessage,
    AsyncResponder,
    Responder,
    middleware::session::RequestSession,
    Error
};
use blake2::{Blake2b, Digest};
use rand;
use ::chrono::Duration;
use futures::prelude::*;

use crate::db::DatabaseValue;
use crate::AppState;
use super::User;
use std::slice;

#[derive(Deserialize, Debug)]
pub struct LoginRequest{
    username: String,
    password: String,
}

#[derive(Serialize, Debug)]
pub struct LoginResponse{
    success: bool,
    error: Option<String>,
}

#[derive(Serialize, Debug)]
pub struct IsLoggedInReply{
    is_logged_in: bool,
}


pub fn login((val,req):(Json<LoginRequest>, HttpRequest<AppState>)) -> Result<HttpResponse,Error>{
    info!("login from user: {}",val.username);
    if let Some(x) = req.state().db.get::<User>(&val.username){
        let gen_hash = Blake2b::new()
            .chain(&val.password)
            .chain(&x.salt)
            .result();

        let mut hash = [0;32];
        hash.copy_from_slice(&gen_hash[..32]);
        trace!("user login provided: {:?}, found: {:?}",hash,x.hash);
        if hash == x.hash{
            let session_key: [u8; 32] = rand::random();
            let cookie_session_key = base64::encode(&session_key);
            req.session().set("logged_in",true)?;
            req.session().set("username",val.username.clone())?;
            req.state().db.set_session(&session_key,&val.username);
            return Ok(HttpResponse::Ok().json(LoginResponse{
                success: true,
                error: None
            }));
        }else{
            return Ok(HttpResponse::Ok().json(LoginResponse{
                success: false,
                error: Some("Invalid password and username combination.".to_string())
            }));
        }
    }
    Ok(HttpResponse::Ok().json(LoginResponse{
        success: false,
        error: Some("Invalid password and username combination.".to_string())
    }))
}

pub fn is_logged_in(req: HttpRequest<AppState>) -> HttpResponse{
    let is_logged_in = req.session().get::<bool>("logged_in")
        .ok()
        .and_then(|x| x)
        .unwrap_or(false);
    HttpResponse::Ok().json(IsLoggedInReply{
        is_logged_in,
    })
}

pub fn logout(req: HttpRequest<AppState>) -> Result<HttpResponse,Error>{
    req.session().clear();
    return Ok(HttpResponse::Ok().json(LoginResponse{
        success: true,
        error: None,
    }))
}
