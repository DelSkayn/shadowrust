use crate::db::DatabaseValue;

pub mod login;
pub mod register;
pub mod info;

use crate::AppState;
use crate::db::Session;
use actix_web::{HttpRequest,HttpResponse,Error};
use actix_web::middleware::session::RequestSession;


pub use self::info::info;

pub use self::login::{
    login,
    logout,
    is_logged_in,
};


pub use self::register::{
    register,
    available,
};

#[derive(Serialize,Deserialize, Debug)]
pub struct User{
    name: String,
    salt: [u8; 32],
    hash: [u8; 32],
}

impl DatabaseValue for User{
    type Key = String;
    const COLUMN_NAME: &'static str = "users";
}

pub fn authenticate<F>(req: &HttpRequest<AppState>, func: F) -> Result<HttpResponse,Error>
where F: FnOnce(&HttpRequest<AppState>) -> Result<HttpResponse,Error>
{
    if  !req.session().get::<bool>("logged_in").unwrap().unwrap_or(false) {
        return Ok(HttpResponse::Unauthorized().reason("Not logged in").finish())
    }
    func(req)
}
