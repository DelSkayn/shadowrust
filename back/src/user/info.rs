use ::actix::prelude::*;
use ::actix_web::{
    HttpRequest,
    HttpResponse,
    Error
};
use actix_web::middleware::session::RequestSession;
use crate::AppState;
use super::authenticate;

#[derive(Serialize,Debug)]
pub struct InfoResponse{
    name: String,
}

pub fn info(req: HttpRequest<AppState>) -> Result<HttpResponse,Error>{
    authenticate(&req,|req|{
        Ok(HttpResponse::Ok().json(InfoResponse{
            name: req.session().get("username").unwrap().unwrap(),
        }))
    })
}
