use actix_web::{
    Json,
    HttpResponse,
    HttpRequest,
};
use blake2::{Blake2b, Digest};
use rand;

use super::User;
use crate::AppState;

#[derive(Deserialize)]
pub struct RegisterData {
    username: String,
    password: String,
}

#[derive(Serialize)]
pub struct RegisterReply<'a> {
    success: bool,
    reason: Option<&'a str>,
}

#[derive(Deserialize)]
pub struct AvailableData {
    username: String,
}

#[derive(Serialize)]
pub struct AvailableReply {
    available: bool,
}

pub fn register((val,req): (Json<RegisterData>,HttpRequest<AppState>)) -> HttpResponse{
    if req.state().db.get::<User>(&val.username).is_some(){
        return HttpResponse::Ok().json(RegisterReply{
            success: false,
            reason: Some("Username already in use."),
        });
    }

    let salt = rand::random();
    let gen_hash = Blake2b::new()
        .chain(&val.password)
        .chain(&salt)
        .result();

    let mut hash = [0;32];
    hash.copy_from_slice(&gen_hash[..32]);
    let user = User{
        name: val.username.clone(),
        salt,
        hash,
    };

    trace!("register hash: {:?}",hash);

    req.state().db.set(&user.name,&user);
    HttpResponse::Ok().json(RegisterReply{
        success: true,
        reason: None,
    })
}

pub fn available((val,req): (Json<AvailableData>,HttpRequest<AppState>)) -> HttpResponse{
    HttpResponse::Ok().json(AvailableReply{
        available: req.state().db.get::<User>(&val.username).is_none()
    })
}
