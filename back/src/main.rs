#![allow(unused_variables)]
#![allow(unused_imports)]
#![feature(futures_api)]

extern crate actix;
extern crate actix_web;
extern crate base64;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[macro_use]
extern crate log;
extern crate pretty_env_logger;

extern crate bincode;
extern crate chrono;
extern crate futures;
extern crate rocksdb;

pub mod db;
pub mod user;
pub mod ws;

#[derive(Clone)]
pub struct AppState {
    db: db::Db,
}

use actix_web::middleware::session::{CookieSessionBackend, RequestSession, SessionStorage};
use actix_web::{fs, http::Method, server, App, HttpRequest, HttpResponse};

fn main() {
    let db = db::Db::new().with_value::<user::User>().build();

    pretty_env_logger::init();

    server::new(move || {
        App::with_state(AppState { db: db.clone() })
            .middleware(SessionStorage::new(db::session::DbSessionBackend::new(
                db.clone(),
            )))
            .route("/api/login", Method::POST, user::login)
            .route("/api/login/info", Method::GET, user::info)
            .route("/api/login", Method::GET, user::is_logged_in)
            .route("/api/logout", Method::GET, user::logout)
            .route("/api/register", Method::POST, user::register)
            .route("/api/register/available", Method::POST, user::available)
            .resource("/ws", |r| {
                trace!("connection for websocket");
                r.route().f(ws::websocket)
            })
            .handler(
                "/",
                fs::StaticFiles::new("static/")
                    .unwrap()
                    .index_file("index.html")
                    .default_handler(|_: &HttpRequest<AppState>| {
                        fs::NamedFile::open("static/index.html")
                    }),
            )
            //.route("/api/logout", Method::GET, login::logout)
            //.route("/api/register", Method::GET, register::register)
            .finish()
    })
    .bind("127.0.0.1:3030")
    .unwrap()
    .run();
}
