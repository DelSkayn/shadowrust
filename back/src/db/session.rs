
use super::{Session,Db};
use crate::AppState;

use chrono::{Duration,Local};
use actix_web::{Error,HttpRequest,HttpResponse};
use actix_web::http::Cookie;
use actix_web::middleware::session::{SessionBackend,SessionImpl};
use actix_web::middleware::Response;
use futures::future::{ok as OkFut, FutureResult};

use std::collections::HashMap;

const COOKIE_NAME: &'static str = "SHRL_SES";

pub struct DbSession{
    db: Db,
    key: [u8; 32],
    set_key: bool,
    changed: bool,
    session: Session,
}

impl SessionImpl for DbSession{
    fn get(&self, key: &str) -> Option<&str>{
        trace!("session: {:#?}",self.session.map);
        self.session.map.get(key).map(|x| x.as_str())
    }

    fn set(&mut self, key: &str, value: String){
        self.changed = true;
        self.session.map.insert(key.to_string(),value);
    }

    fn remove(&mut self,key: &str){
        self.changed = true;
        self.session.map.remove(key);
    }

    fn clear(&mut self){
        self.changed = true;
        self.session.map.clear();
    }

    fn write(&self,mut resp: HttpResponse) -> Result<Response,Error>{
        if self.changed {
            if self.session.map.len() != 0{
                self.db.set::<Session>(&self.key,&self.session);
                if self.set_key {
                    let name = COOKIE_NAME.to_string();
                    let value = base64::encode(&self.key);
                    resp.add_cookie(&Cookie::build(name,value)
                                    .path("/")
                                    .secure(false)
                                    .permanent()
                                    .finish()).unwrap();
                }
            }else{
                self.db.remove::<Session>(&self.key);
                if !self.set_key{
                    resp.add_cookie(&Cookie::build(COOKIE_NAME,"deleted")
                                    .path("/")
                                    .secure(false)
                                    .max_age(Duration::milliseconds(1))
                                    .finish()).unwrap();
                }
            }
        }
        Ok(Response::Done(resp))
    }
}

pub struct DbSessionBackend{
    db: Db,
}

impl DbSessionBackend{
    pub fn new(db: Db) -> Self{
        DbSessionBackend{
            db,
        }
    }

}

impl SessionBackend<AppState> for DbSessionBackend{
    type Session = DbSession;
    type ReadFuture = FutureResult<DbSession,Error>;

    fn from_request(&self, req: &mut HttpRequest<AppState>) -> Self::ReadFuture{
        if let Ok(cookies) = req.cookies(){
            for cookie in cookies.iter(){
                if cookie.name() == COOKIE_NAME {
                    let mut res = [0;32];
                    let session = base64::decode(cookie.value())
                        .ok()
                        .and_then(|x| if x.len() != 32 { None } else {Some(x)} )
                        .map(|x|{
                            res.copy_from_slice(&x);
                            res
                        })
                        .and_then(|x| self.db.get_session(&x));
                    if let Some(x) = session{
                        return OkFut(DbSession{
                            db: self.db.clone(),
                            key: res,
                            set_key: false,
                            changed: false,
                            session: x,
                        })
                    }
                }
            }
        }
        OkFut(DbSession{
            db: self.db.clone(),
            changed: false,
            key: rand::random(),
            set_key: true,
            session: Session{
                time: Local::now(),
                map: HashMap::new(),
            }
        })
    }
}

