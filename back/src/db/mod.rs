use serde::{Serialize,Deserialize,de::DeserializeOwned};
use rocksdb::{ColumnFamily, ColumnFamilyDescriptor, Options, DB};
use chrono::{DateTime, Local};
use bincode;
use std::collections::HashMap;

use std::sync::Arc;

pub mod session;

pub trait DatabaseValue: Serialize + DeserializeOwned{
    type Key: Serialize + DeserializeOwned;
    const COLUMN_NAME: &'static str;

    fn from_bytes(b: &[u8]) -> Self{
        bincode::deserialize(b).unwrap()
    }

    fn to_bytes(&self) -> Vec<u8>{
        bincode::serialize(self).unwrap()
    }
}

#[derive(Serialize,Deserialize)]
pub struct Session{
    time: DateTime<Local>,
    map: HashMap<String,String>
}

impl DatabaseValue for Session{
    const COLUMN_NAME:&'static str = "sessions";
    type Key = [u8; 32];
}

pub struct DbBuilder{
    columns: Vec<&'static str>,
}

impl DbBuilder{
    pub fn new() -> DbBuilder{
        DbBuilder{
            columns: Vec::new(),
        }
    }

    pub fn with_value<T: DatabaseValue>(mut self) -> Self{
        self.columns.push(T::COLUMN_NAME);
        self
    }

    pub fn build(mut self) -> Db{
        self = self.with_value::<Session>();
        let columns = self.columns
            .drain(..)
            .map(|e|{
                ColumnFamilyDescriptor::new(e,Options::default())
            }).collect();

        let mut db_opts = Options::default();
        db_opts.create_missing_column_families(true);
        db_opts.create_if_missing(true);

        let db = DB::open_cf_descriptors(&db_opts, "shadowroll.db", columns).unwrap();

        Db(Arc::new(db))
    }
}

#[derive(Clone)]
pub struct Db(Arc<DB>);

impl Db {
    pub fn new() -> DbBuilder {
        DbBuilder::new()
    }

    pub fn get<T>(&self, key: &T::Key) -> Option<T>
        where T: DatabaseValue,
    {
        let key = bincode::serialize(key).unwrap();
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        self.0
            .get_cf(handle,&key)
            .unwrap()
            .map(|e| T::from_bytes(e.as_ref()))
    }

    pub fn set<T>(&self,key: &T::Key, value: &T)
        where T: DatabaseValue,
    {
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        let key = bincode::serialize(key).unwrap();
        let value = value.to_bytes();
        self.0.put_cf(handle,&key,&value).unwrap();
    }

    pub fn remove<T>(&self,key: &T::Key)
        where T: DatabaseValue,
    {
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        let key = bincode::serialize(key).unwrap();
        self.0.delete_cf(handle,&key).unwrap();
    }

    pub fn set_session(&self,key: &[u8;32], username: &str){
        let mut map = HashMap::new();
        map.insert("name".to_string(),username.to_string());
        self.set(key,&Session{
            time: Local::now(),
            map,
        });
    }

    pub fn get_session(&self,key: &[u8;32]) -> Option<Session>{
        if let Some(mut x) = self.get::<Session>(key){
            if x.time.signed_duration_since(Local::now()).num_days() >= 1{
                self.remove::<Session>(key);
                None
            }else{
                x.time = Local::now();
                self.set(key, &x);
                Some(x)
            }
        }else{
            None
        }
    }
    pub fn remove_session(&self,key: &[u8;32]){
        self.remove::<Session>(key);
    }
}
