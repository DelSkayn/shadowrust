use warp;
use crate::login::session;

fn filter(db: Db) -> impl Filter{
    let path = path!("api" / "channel").and(warp::path::end());
    path.and(warp::ws2())
        .and(session(db))
        .map(|ws: warp::ws::Ws2|{
            ws.on_upgrade(|websocket|{
                let *
            })
        })
}

fn user_connected(ws: WebSocket)
