use base64;
use blake2::{Blake2b, Digest};
use crate::db::Db;
use crate::db::{Session,User};

use chrono::Local;
use http::response::Builder;
use http::status::StatusCode;
use serde_json;
use warp::filters::BoxedFilter;
use warp::Filter;
use warp::Rejection;

use std::mem;

#[derive(Deserialize, Debug)]
pub struct LoginData {
    username: String,
    password: String,
}

#[derive(Deserialize, Debug)]
pub struct LogoutData {
    username: String,
}

#[derive(Serialize, Clone)]
pub struct IsLoggedInReply {
    is_logged_in: bool,
}

#[derive(Serialize, Clone)]
pub struct LoginReply {
    success: bool,
    session: Option<[u32; 4]>,
    error: Option<String>,
}

#[derive(Serialize, Clone)]
pub struct LogoutReply {
    success: bool,
    error: Option<String>,
}

pub fn login(db: Db, login_data: LoginData) -> impl warp::Reply {
    info!("login from user: '{}'", login_data.username);
    if let Some(x) = db.get::<User>(&login_data.username) {
        let hash = Blake2b::new()
            .chain(login_data.password)
            .chain(&x.salt)
            .result();

        debug!("hash: {:#?}", hash);

        if &hash[0..32] == x.hash {
            info!("login succeeded");
            let session_key: [u8; 32] = rand::random();
            let session = Session {
                name: login_data.username.clone(),
                time: Local::now(),
            };
            db.set(&session_key, &session);
            let key = "Set-Cookie";
            let value = format!(
                "EXAUTH={}; SameSite=Strict; HttpOnly",
                base64::encode(&session_key)
            );
            debug!("{}", value);
            Builder::new()
                .header(key, value)
                .header("Content-Type", "application/json")
                .body(
                    serde_json::to_string(&LoginReply {
                        success: true,
                        session: None,
                        error: None,
                    })
                        .unwrap(),
                )
                .unwrap()
        } else {
            Builder::new()
                .header("Content-Type", "application/json")
                .body(
                    serde_json::to_string(&LoginReply {
                        success: false,
                        session: None,
                        error: Some("invalid password".to_string()),
                    })
                        .unwrap(),
                )
                .unwrap()
        }
    } else {
        Builder::new()
            .header("Content-Type", "application/json")
            .body(
                serde_json::to_string(&LoginReply {
                    success: false,
                    session: None,
                    error: Some("user not found".to_string()),
                })
                    .unwrap(),
            )
            .unwrap()
    }
}

pub fn is_logged_in(session: Option<Session>) -> impl warp::Reply {
    warp::reply::json(&IsLoggedInReply {
        is_logged_in: session.is_some(),
    })
}

pub fn logout(db: Db, cookie: String) -> impl warp::Reply {
    let res_vec = base64::decode(&cookie);
    let res_vec = match res_vec {
        Ok(x) => x,
        Err(_) => {
            return warp::reply::json(&LogoutReply {
                success: false,
                error: Some("invalid session key".to_string()),
            })
        }
    };
    if res_vec.len() < 32 {
        return warp::reply::json(&LogoutReply {
            success: false,
            error: Some("invalid session key".to_string()),
        });
    }
    let mut res: [u8; 32] = [0; 32];
    for i in 0..32 {
        res[i] = res_vec[i];
    }
    db.remove::<Session>(&res);
    warp::reply::json(&LogoutReply {
        success: true,
        error: None,
    })
}

pub fn session(db: Db) -> impl Filter<Extract = (Session,), Error = Rejection> + Clone {
    try_session(db)
        .and_then(|x: Option<Session>|{
            x.ok_or(warp::reject::not_found())
        })
}

pub fn try_session(db: Db) -> impl Filter<Extract = (Option<Session>,), Error = Rejection> + Clone {
    warp::any()
        .and(warp::cookie("EXAUTH"))
        .map(move |x| (x,db.clone()))
        .and_then(|data: (String,Db)| {
            let (cookie,db) = data;
            base64::decode(&cookie)
                .map_err(|_|{
                    warp::reject::not_found()
                })
                .and_then(|c| {
                    if c.len() < 32 {
                        //TODO change to proper error;
                        return Err(warp::reject::not_found());
                    }
                    let mut res: [u8; 32] = [0; 32];
                    for i in 0..32 {
                        res[i] = c[i]
                    }
                    Ok(db.session(res))
                })
        })
}
