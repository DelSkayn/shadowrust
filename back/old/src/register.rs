use blake2::{Blake2b, Digest};
use rand;
use warp;

use crate::db::{Db, User};


#[derive(Deserialize)]
pub struct AvailableData {
    username: String,
}

#[derive(Serialize)]
pub struct AvailableReply {
    available: bool,
}

pub fn register(db: Db, register: RegisterData) -> impl warp::Reply {
    if db.get::<User>(&register.username).is_some() {
        warp::reply::json(&RegisterReply {
            success: false,
            reason: Some("username already in use"),
        })
    } else {
        let salt = rand::random();
        let gen_hash = Blake2b::new()
            .chain(register.password)
            .chain(salt)
            .result();

        let mut hash = [0; 32];
        for i in 0..32{
            hash[i] = gen_hash[i];
        }

        let user = User {
            name: register.username,
            salt,
            hash,
        };

        db.set::<User>(&user.name,&user);
        warp::reply::json(&RegisterReply {
            success: true,
            reason: None,
        })
    }
}

pub fn available(db: Db, a: AvailableData) -> impl warp::Reply{
    warp::reply::json(&AvailableReply{
        available: db.get::<User>(&a.username).is_none(),
    })
}
