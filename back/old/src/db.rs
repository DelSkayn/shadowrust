use bincode;
use chrono::{DateTime, Local};
use rocksdb::{ColumnFamily, ColumnFamilyDescriptor, Options, DB};
use serde::{Serialize,Deserialize,de::DeserializeOwned};

use std::collections::HashMap;
use std::sync::{Arc, RwLock};


pub trait DatabaseValue: Serialize + DeserializeOwned{
    const COLUMN_NAME: &'static str;

    type Key: Serialize + DeserializeOwned;

    fn into_data(&self) -> Vec<u8>{
        bincode::serialize(self).unwrap()
    }

    fn from_data(data: &[u8]) -> Self{
        bincode::deserialize(data).unwrap()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct User {
    pub name: String,
    pub salt: [u8; 32],
    pub hash: [u8; 32],
}

impl DatabaseValue for User{
    const COLUMN_NAME: &'static str = "users";

    type Key = String;
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Session {
    pub name: String,
    pub time: DateTime<Local>,
}

impl DatabaseValue for Session{
    const COLUMN_NAME: &'static str = "sessions";

    type Key = [u8; 32];
}

pub struct DbBuilder{
    columns: Vec<&'static str>,
}

impl DbBuilder{
    pub fn new() -> DbBuilder{
        DbBuilder{
            columns: Vec::new(),
        }
    }

    pub fn with_value<T: DatabaseValue>(mut self) -> Self{
        self.columns.push(T::COLUMN_NAME);
        self
    }

    pub fn build(mut self) -> Db{
        let columns = self.columns
            .drain(..)
            .map(|e|{
                ColumnFamilyDescriptor::new(e,Options::default())
            }).collect();

        let mut db_opts = Options::default();
        db_opts.create_missing_column_families(true);
        db_opts.create_if_missing(true);

        let db = DB::open_cf_descriptors(&db_opts, "shadowroll.db", columns).unwrap();

        Db(Arc::new(db))
    }
}


#[derive(Clone)]
pub struct Db(Arc<DB>);

impl Db {
    pub fn new() -> DbBuilder {
        DbBuilder::new()
    }

    pub fn get<T>(&self, key: &T::Key) -> Option<T>
        where T: DatabaseValue,
    {
        let key = bincode::serialize(key).unwrap();
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        self.0
            .get_cf(handle,&key)
            .unwrap()
            .map(|e| T::from_data(e.as_ref()))
    }

    pub fn set<T>(&self,key: &T::Key, value: &T)
        where T: DatabaseValue,
    {
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        let key = bincode::serialize(key).unwrap();
        let value = value.into_data();
        self.0.put_cf(handle,&key,&value).unwrap();
    }

    pub fn remove<T>(&self,key: &T::Key)
        where T: DatabaseValue,
    {
        let handle = self.0.cf_handle(T::COLUMN_NAME).unwrap();
        let key = bincode::serialize(key).unwrap();
        self.0.delete_cf(handle,&key).unwrap();
    }

    // TODO find a better way to handle this.
    pub fn session(&self, key: [u8; 32]) -> Option<Session> {
        let sessions = self.0.cf_handle("sessions").unwrap();
        let res = self
            .0
            .get_cf(sessions, &key)
            .unwrap()
            .map(|e| bincode::deserialize::<Session>(&*e).unwrap());
        if let Some(x) = res {
            if x.time.signed_duration_since(Local::now()).num_days() >= 1 {
                self.0.delete_cf(sessions, &key).unwrap();
                None
            } else {
                Some(x)
            }
        } else {
            None
        }
    }
}
