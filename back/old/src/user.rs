
use base64;
use warp;

use crate::db::Db;

#[derive(Debug,Serialize)]
pub struct UsernameReply<'a>{
    successfull: bool,
    error: Option<&'a str>,
    name: Option<String>,
}

pub fn username(db:Db,cookie: Option<String>) -> impl Reply{
    let cookie = match cookie{
        Some(x) => x,
        Err(_) => return warp::reply::json(UsernameReply{
            sucessfull: false,
            error: Some("not logged in");
            name: None,
        })
    }
    let cookie = match base64::decode(cookie){
        Ok(x) => x,
        Err(_) => return warp::reply::json(UsernameReply{
            sucessfull: false,
            error: Some("Invalid session cookie");
            name: None,
        })
    }
    let session = db.get_session()
}
