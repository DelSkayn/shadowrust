#![allow(unused_variables)]
#![allow(unused_imports)]


extern crate chrono;
extern crate fern;
extern crate rand;
extern crate base64;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json as json;
#[macro_use]
extern crate warp;

#[macro_use]
extern crate log;
extern crate pretty_env_logger;

extern crate rocksdb;
extern crate bincode;
extern crate http;

use warp::Filter;

use std::env;

mod db;
use self::db::{Db,User,Session};

mod login;
mod register;

fn main() {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "INFO");
    }
    pretty_env_logger::init();
    info!("starting server!");

    let db = Db::new()
        .with_value::<User>()
        .with_value::<Session>()
        .build();
    let db_clone = db.clone();
    let db_filter = warp::any().map(move || db_clone.clone());

    let login_index = path!("api" / "login").and(warp::path::end());
    let logout_index = path!("api" / "logout").and(warp::path::end());
    let available_index = path!("api" / "register" / "available").and(warp::path::end());
    let register_index = path!("api" / "register").and(warp::path::end());

    let hello = path!("hello" / String).map(|name| format!("Hello, {}", name));

    let login = warp::post2()
        .and(login_index)
        .and(db_filter.clone())
        .and(warp::body::json())
        .map(login::login);

    let is_logged_in = warp::get2()
        .and(login_index)
        .and(login::try_session(db.clone()))
        .map(login::is_logged_in);

    let logout = warp::get2()
        .and(logout_index)
        .and(db_filter.clone())
        .and(warp::cookie("EXAUTH"))
        .map(login::logout);

    let register = warp::post2()
        .and(register_index.and(warp::path::end()))
        .and(db_filter.clone())
        .and(warp::body::json())
        .map(register::register);

    let available = warp::post2()
        .and(available_index)
        .and(db_filter.clone())
        .and(warp::body::json())
        .map(register::available);

    let routes = login
        .or(is_logged_in)
        .or(logout)
        .or(hello)
        .or(register)
        .or(available);

    warp::serve(routes).run(([127, 0, 0, 1], 3030));
}
